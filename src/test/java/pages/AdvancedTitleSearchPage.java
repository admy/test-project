package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;
import java.util.Random;

public class AdvancedTitleSearchPage extends BasePage {

    public AdvancedTitleSearchPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "header")
    public WebElement advancedSearchTitle;

    @FindBy(name = "user_rating-min")
    public WebElement userRatingDropdown;

    @FindBy(name = "countries")
    public WebElement countries;

    @FindBy(xpath = "//button[contains(text(),'Search')]")
    public WebElement searchButton;


    @Step
    public String readAdvancedSearchTitle() {
        String txt = advancedSearchTitle.getText();
        return txt;
    }

    @Step
    public AdvancedTitleSearchPage setTitleTypeCheckbox(String titleType) {
        driver.findElement(By.cssSelector("input[value=" + titleType + "][name=\"title_type\"]"));
        return this;
    }

    @Step
    public AdvancedTitleSearchPage selectOnUserRatingDropdown(String userRating) {
        Select select = new Select(userRatingDropdown);
        select.selectByVisibleText(userRating);
        return this;
    }

    @Step
    public AdvancedTitleSearchPage setGenresCheckbox(String genre) {
        driver.findElement(By.cssSelector("input[value=" + genre + "][name=\"genres\"]"));
        return this;
    }

    @Step
    public AdvancedTitleSearchPage selectCountries(String country) {
        Select select = new Select(countries);
        select.selectByValue(country);
        return this;
    }

    @Step
    public AdvancedTitleSearchPage setAdultTitlesCheckbox(String adultTitle) {
        driver.findElement(By.cssSelector("input[value=" + adultTitle + "][name=\"adult\"]"));
        return this;
    }

    @Step
    public AdvancedTitleSearchPage clickOnSearchForMovieButton() {
        searchButton.click();
        return this;
    }

    @Step
    public void setFilters(String titleType, String userRating, String genres, String country, String adultTitle) {
        setTitleTypeCheckbox(titleType)
                .selectOnUserRatingDropdown(userRating)
                .setGenresCheckbox(genres)
                .selectCountries(country)
                .setAdultTitlesCheckbox(adultTitle)
                .clickOnSearchForMovieButton();
    }

    @Step
    public Integer countResultMovieList() {
        List<WebElement> allMovieInList = driver.findElements(By.cssSelector(".lister-item-image"));
        Integer countMovies = allMovieInList.size();
        return countMovies;
    }

    @Step
    public void addToWatchList(Integer index) {
        Random rand = new Random();
        List<WebElement> addButtons = driver.findElements(By.cssSelector(".wl-ribbon"));
        for (int i = 0; i<index; i++){
            addButtons.get(i).click();
        }
    }

    public AdvancedTitleSearchPage click (Integer index){
        driver.findElements(By.cssSelector(".wl-ribbon")).get(index).click();
        return this;
    }
}

