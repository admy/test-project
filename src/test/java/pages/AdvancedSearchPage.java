package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class AdvancedSearchPage extends BasePage {
    public AdvancedSearchPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "header")
    public WebElement advancedSearchTitle;

    @Step
    public String readAdvancedSearchTitle(){
        String txt = advancedSearchTitle.getText();
        return txt;
    }

}
