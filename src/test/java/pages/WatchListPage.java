package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class WatchListPage extends BasePage {

    public WatchListPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "navWatchlistMenu")
    WebElement watchlistMenuLink;

    @Step
    public WatchListPage navigateToWatchListPage() {
        watchlistMenuLink.click();
        return this;
    }

    @Step
    public Integer countMoviesInWatchList() {
        List<WebElement> movieInList = driver.findElements(By.cssSelector(".clearfix"));
        Integer countMovies = movieInList.size();
        return countMovies;
    }

    @Step
    public List<String> readMovieTitle() {
        List<String> actualResult = new ArrayList<String>();
        List<WebElement> movieTitles = driver.findElements(By.cssSelector(".lister-item-header a"));
        for (int i = 0; i < movieTitles.size(); i++) {
            actualResult.add(movieTitles.get(i).getText());
        }
        return actualResult;
    }
}