package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SignInPage extends BasePage {
    public SignInPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".list-group-item")
    public List<WebElement> signInItems;

    @FindBy(css = ".a-box-inner .a-spacing-small")
    public WebElement signInTitle;

    @FindBy(id = "ap_email")
    public WebElement emailField;

    @FindBy(id = "ap_password")
    public WebElement passwordField;

    @FindBy(id = "signInSubmit")
    public WebElement signInButton;


    @Step
    public SignInPage selectSignInType(Integer index) {
        signInItems.get(index).click();
        return this;
    }

    @Step
    public String readSignInTitle() {
        String txt = signInTitle.getText();
        return txt;
    }

    @Step
    public SignInPage setEmailField(String email) {
        emailField.sendKeys(email);
        return this;
    }

    @Step
    public SignInPage setPasswordField(String pass) {
        passwordField.sendKeys(pass);
        return this;
    }

    @Step
    public SignInPage clickOnSignInButton() {
        signInButton.click();
        return this;
    }

    public void signIn(String email, String password) {
        setEmailField(email)
                .setPasswordField(password)
                .clickOnSignInButton();
    }
}