package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.htmlelements.annotations.Name;

import java.util.List;

public class IMDbHomePage extends BasePage {
    public IMDbHomePage(WebDriver driver) {
        super(driver);
    }

    //URL to website IMDb:
    WebDriverWait wait = new WebDriverWait(driver, 5);

    public static String baseUrl = "https://www.imdb.com/";

    //IMDb menu panel web elements:

    @Name("IMDb main panel")
    @FindBy(id = "navbar-query")
    public WebElement serachField;

    @FindBy(id = "navbar-submit-button")
    public WebElement searchButton;

    @FindBy(id = "quicksearch")
    public WebElement quickSearchDropdown;

    @FindBy(id = "nblogin")
    public WebElement signInOptionsLink;

    @FindBy(css = ".poster")
    public WebElement poster;


    @FindBy(css = ".wl-ribbon")
    public WebElement addMovieToWatchListButton;

    @FindBy(css = "#nb_personal .singleLine a")
    public WebElement userProfileName;

    //Action methods:

    @Step
    public IMDbHomePage navigateToHomePage() {
        driver.get(baseUrl);
        return this;
    }

    @Step
    public IMDbHomePage writeTextInSerchField(String movieName) {
        serachField.sendKeys(movieName);
        return this;
    }

    @Step
    public IMDbHomePage clickSubmitSearchButton() {
        searchButton.click();
        return this;
    }

    @Step
    public IMDbHomePage clickOnQuickSearchDropdown() {
        Select select = new Select(quickSearchDropdown);
        select.selectByValue("/search/");
        return this;
    }

    @Step
    public IMDbHomePage clickOnSignInOptionsLink() {
        signInOptionsLink.click();
        return this;
    }

    @Step
    public IMDbHomePage clickOnPoster() {
        new WebDriverWait(driver, 5).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(poster))).click();
        return this;
    }

    @Step
    public IMDbHomePage clickOnAddMovieToWatchListButton() {
        new WebDriverWait(driver, 5).until(ExpectedConditions.refreshed(ExpectedConditions.elementToBeClickable(addMovieToWatchListButton))).click();
        return this;
    }

    @Step
    public IMDbHomePage addMovieToWatchlist(List<String> filmNames) {
        for (int i = 0; i < filmNames.size(); i++) {
            WebElement searchField = driver.findElement(By.id("navbar-query"));
            searchField.sendKeys(filmNames.get(i));
            clickOnPoster();
            clickOnAddMovieToWatchListButton();
        }
        return this;
    }

    @Step
    public String readUserProfileName() {
        String txt = userProfileName.getText();
        return txt;
    }

}
