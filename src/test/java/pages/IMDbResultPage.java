package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class IMDbResultPage extends BasePage {
    public IMDbResultPage(WebDriver driver) {
        super(driver);
    }

    //Result page elements
    @FindBy(className = "findHeader")
    public WebElement resultTitle;

    @FindBy(xpath = "//a[contains(text(),'Advanced Title Search')]")
    public WebElement advancedTitleSearch;

    @FindBy(css = ".primary_photo")
    public List<WebElement> resultText;

    @FindBy(xpath = "//h1[@itemprop='name']")
    public WebElement movieTitle;

    @Step
    public IMDbResultPage clickAdvancedTitleSearchLink() {
        advancedTitleSearch.click();
        return this;
    }

    @Step
    public IMDbResultPage clickOnResultMovieTitle(Integer index) {
        resultText.get(index).click();
        return this;
    }
}
