package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.IMDbHomePage;
import pages.IMDbResultPage;


public class SimpleSearchTest extends BaseTest {

    @Test(priority = 1)
    public void verifySearchResultsPage() {
        new IMDbHomePage(driver)
                .navigateToHomePage()
                .writeTextInSerchField("Deadpool")
                .clickSubmitSearchButton();
        IMDbResultPage imDbResultPage = new IMDbResultPage(driver)
                .clickOnResultMovieTitle(1);
        Assert.assertEquals(imDbResultPage.movieTitle.getText(), "Deadpool ");
    }
}
