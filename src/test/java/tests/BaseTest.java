package tests;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;
import pages.IMDbHomePage;
import pages.SignInPage;

import java.net.MalformedURLException;
import java.net.URL;


public class BaseTest {
    public WebDriver driver;
    public WebDriverWait wait;

    String SignInURL = "https://www.imdb.com/registration/signin";
    String email = "yrdy@maildrop.cc";
    String pass = "12345678";

    @BeforeSuite
    public void setup(){
        WebDriverManager.config().setTargetPath("target");
    }

    @Parameters({"browser"})
    @BeforeTest
    public void classLevelSetup(@Optional("chrome") String browserType) throws MalformedURLException {

        switch (browserType) {
            case "chrome":

                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case "selenoid" :
                driver = new RemoteWebDriver(new URL("http://35.187.27.180:4444/wd/hub"), new ChromeOptions());
                break;
            default:
                ChromeDriverManager.getInstance().setup();
                driver = new ChromeDriver();
                break;
        }
        wait = new WebDriverWait(driver, 15);
        driver.manage().window().maximize();
    }

    @AfterTest
    public void teardown() {
        driver.quit();
    }
}
