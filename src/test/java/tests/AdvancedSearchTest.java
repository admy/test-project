package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.AdvancedSearchPage;
import pages.AdvancedTitleSearchPage;
import pages.IMDbHomePage;
import pages.IMDbResultPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;


public class AdvancedSearchTest extends BaseTest {

    @Test(priority = 1)
    public void verifyClickOnQuickSearch() {
        new IMDbHomePage(driver)
                .navigateToHomePage()
                .clickOnQuickSearchDropdown();
        new IMDbResultPage(driver).clickAdvancedTitleSearchLink();
        AdvancedTitleSearchPage advancedTitleSearchPage = new AdvancedTitleSearchPage(driver);
        advancedTitleSearchPage.setFilters("feature", "6.0", "horror", "ua", "include");
        assertThat(advancedTitleSearchPage.countResultMovieList(), greaterThan(1));
    }
}
