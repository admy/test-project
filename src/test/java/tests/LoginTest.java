package tests;

import org.testng.annotations.Test;
import pages.IMDbHomePage;
import pages.SignInPage;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;


public class LoginTest extends BaseTest {
    String userName = "test";

    @Test(priority = 0)
    public void verifyAddMovies() {
        IMDbHomePage imDbHomePage = new IMDbHomePage(driver);
        imDbHomePage.navigateToHomePage();
        imDbHomePage.clickOnSignInOptionsLink();
        SignInPage signInPage = new SignInPage(driver)
                .selectSignInType(3);
        signInPage.signIn(email, pass);
        assertThat(userName, equalTo(imDbHomePage.readUserProfileName()));
    }
}

