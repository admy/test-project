package tests;

import org.testng.annotations.Test;
import pages.IMDbHomePage;
import pages.SignInPage;
import pages.WatchListPage;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class AddingMoviesToWatchList extends BaseTest {
    List<String> filmNames = Arrays.asList("The Pianist", "Braveheart");

    @Test(priority = 0)
    public void verifyAddMovies() {
        new IMDbHomePage(driver)
                .navigateToHomePage()
                .clickOnSignInOptionsLink();
        SignInPage signInPage = new SignInPage(driver)
                .selectSignInType(3);
        signInPage.signIn(email, pass);
        WatchListPage watchListPage = new WatchListPage(driver);
        new IMDbHomePage(driver).addMovieToWatchlist(filmNames);
        watchListPage.navigateToWatchListPage();

        assertThat(filmNames, equalTo(watchListPage.readMovieTitle()));
    }
}
